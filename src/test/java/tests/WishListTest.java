package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;
import pageobjects.WomenPage;

public class WishListTest extends BaseTest {

    @Test
    void shouldAddRandomProductsToWishListWhenUserIsLoggedIn() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wp.pl", "mojtest");
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        Assertions.assertTrue(myAccountPage.orderHistoryOptionIsDisplayed());
        myAccountPage.createAWishList("Favourites");
        Assertions.assertTrue(myAccountPage.wishListCreated());
        homepage.goToWomenPage();
        WomenPage womenPage = new WomenPage(driver, wait);
        womenPage.addRandomProductsToWishlist(3);
        womenPage.goToAccountWishListOption();
        Assertions.assertEquals("3", myAccountPage.getNumberOfProductsInWishList());
        Thread.sleep(8000);
    }

    @Test
    void shouldDisplayErrorByAddingRandomProductsToWishlistIfUserNotLoggedIn() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        homepage.goToWomenPage();
        WomenPage womenPage = new WomenPage(driver, wait);
        womenPage.addRandomProductsToWishlist(3);
        Assertions.assertEquals("You must be logged in to manage your wishlist.", womenPage.getErrorMessageByAddingProductToWishlist());
    }
}
