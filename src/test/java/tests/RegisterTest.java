package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;
import pageobjects.RegisterPage;

public class RegisterTest extends BaseTest {


    @Test
    void shouldRegisterRandomUserSuccessfully() throws InterruptedException {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.createAnAccount();
        RegisterPage registerPage = new RegisterPage(driver, wait);
        registerPage.fillInRegistrationForm();
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        Assertions.assertTrue(myAccountPage.orderHistoryOptionIsDisplayed());
    }

    @Test
    void shouldDisplayErrorMessageIfRequiredFieldOrFieldsNotFilledIn() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.createAnAccount();
        RegisterPage registerPage = new RegisterPage(driver, wait);
        registerPage.giveNumberOfRequiredFieldsThatStayEmpty(3);
        Assertions.assertTrue(registerPage.alertBannerIsDisplayed());
        Thread.sleep(15000);
    }

    @Test
    void shouldShowErrorWhenWrongFormatOfZipCodeProvided() {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.createAnAccount();
        RegisterPage registerPage = new RegisterPage(driver, wait);
        registerPage.provideZipCode("56780-000");
        Assertions.assertEquals("The Zip/Postal code you've entered is invalid. It must follow this format: 00000", registerPage.getErrorMessageByWrongDataProvided());
    }

    @Test
    void shouldShowErrorWhenPasswordDoesNotMatchCriteria() {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.createAnAccount();
        RegisterPage registerPage = new RegisterPage(driver, wait);
        registerPage.providePassword("abcd");
        Assertions.assertEquals("passwd is invalid.", registerPage.getErrorMessageByWrongDataProvided());
    }
}
