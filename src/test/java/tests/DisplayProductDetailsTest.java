package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.TshirtsPage;


public class DisplayProductDetailsTest extends BaseTest {

    @Test
    void shouldShowQuickView()  {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        TshirtsPage tshirtsPage = homepage.goToTshirtsPage();
        tshirtsPage.getQuickViewOfProduct();
        Assertions.assertTrue(tshirtsPage.quickViewIsDisplayed());

    }

    @Test
    void shouldCheckQuickViewOptions() {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        TshirtsPage tshirtsPage = homepage.goToTshirtsPage();
        tshirtsPage.getQuickViewOfProduct();
        Assertions.assertTrue(tshirtsPage.quickViewIsDisplayed());
        tshirtsPage.changeQuantityOfProductInQuickView("+", 4);
        Assertions.assertEquals("5", tshirtsPage.getCurrentQuantity());
        tshirtsPage.changeQuantityOfProductInQuickView("-", 3);
        Assertions.assertEquals("2", tshirtsPage.getCurrentQuantity());
        tshirtsPage.chooseSize("L");
        Assertions.assertEquals("L", tshirtsPage.chosenSizeIsDisplayed());


    }

    @Test
    void shouldDisplayMoreDetailsAboutProduct()  {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        TshirtsPage tshirtsPage = homepage.goToTshirtsPage();
        tshirtsPage.goToMoreDetails();
        Assertions.assertEquals(tshirtsPage.displayExpectedSections(),tshirtsPage.getSectionNames());

    }
}

