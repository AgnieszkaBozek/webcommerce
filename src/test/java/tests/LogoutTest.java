package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;

public class LogoutTest extends BaseTest {


    @Test
    void shouldLogoutFromAccountSuccessfully() throws InterruptedException {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wp.pl", "mojtest");
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        Assertions.assertTrue(myAccountPage.orderHistoryOptionIsDisplayed());
        myAccountPage.signOut();
        Assertions.assertTrue(loginPage.createAnAccountButtonIsDisplayed());
    }
}
