package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.DressesPage;
import pageobjects.HomePage;


public class AddToCartTest extends BaseTest {


    @Test
    void shouldAddRandomProductsToCart() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        DressesPage dressesPage = homepage.goToDressesPage();
        dressesPage.addRandomProductsToCart(4);
        Assertions.assertEquals("4", dressesPage.getItemsNumber());
    }

    @Test
    void shouldCheckTotalPriceOfProductsInCart() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        DressesPage dressesPage = homepage.goToDressesPage();
        dressesPage.checkPriceForAddedRandomProducts(3);
        dressesPage.goToShoppingCartPage();
        Assertions.assertEquals(dressesPage.getExpectedCartValue(), dressesPage.getCurrentTotalValue());
    }
}







