package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;

public class LoginTest extends BaseTest {

    @Test
    void shouldLoginWithCorrectData() throws InterruptedException {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wp.pl", "mojtest");
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        Assertions.assertTrue(myAccountPage.orderHistoryOptionIsDisplayed());
    }

    @Test
    void shouldDisplayErrorWhenLoginWithIncorrectPassword() throws InterruptedException {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wp.pl", "mojtesttgdg");
        Assertions.assertEquals("Authentication failed.", loginPage.getErrorTextByLoginAttempt());
    }

    @Test
    void shouldDisplayErrorWhenLoginWithIncorrectEmailAddress() throws InterruptedException {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wprrr.pl", "mojtest");
        Assertions.assertEquals("Authentication failed.", loginPage.getErrorTextByLoginAttempt());
    }

    @Test
    void shouldDisplayErrorWhenLoginWithNoPassword() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wp.pl", "");
        Assertions.assertEquals("Password is required.", loginPage.getErrorTextByLoginAttempt());
    }

    @Test
    void shouldDisplayErrorWhenLoginWithNoEmailAddress() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("", "mojtest");
        Assertions.assertEquals("An email address required.", loginPage.getErrorTextByLoginAttempt());
    }
}
