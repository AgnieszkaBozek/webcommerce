package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;


public class NavigationMenuTest extends BaseTest {


    @Test
    void shouldCheckIfNavigationMenuTabsAreVisible() {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        Assertions.assertEquals(3, homepage.countMenuTabs());
    }

    @Test
    void shouldCheckIfSubCategoriesAreDisplayed() throws InterruptedException {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        homepage.getSubcategories("women");
        Assertions.assertTrue(homepage.subcategoriesAreVisible(0));
    }
}


