package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import pageobjects.*;

public class OrderingTest extends BaseTest {

    @Test
    void shouldOrderRandomProducts() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wp.pl", "mojtest");
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        Assertions.assertTrue(myAccountPage.orderHistoryOptionIsDisplayed());
        DressesPage dressesPage = homepage.goToDressesPage();
        dressesPage.addRandomProductsToCart(2);
        Assertions.assertEquals("2", dressesPage.getItemsNumber());
        dressesPage.goToShoppingCartPage();
        ShoppingCartPage shoppingCartPage = new ShoppingCartPage(driver, wait);
        shoppingCartPage.proceedToCheckout();
        shoppingCartPage.acceptTermsAndConditions();
        shoppingCartPage.choosePaymentMethod("check");
        shoppingCartPage.confirmOrder();
        Assertions.assertTrue(shoppingCartPage.orderCompleteMessageIsDisplayed());
    }
}
