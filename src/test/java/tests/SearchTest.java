package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.SortParameters;


public class SearchTest extends BaseTest {

    @Test
    void shouldFindExistingProducts()  {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        homepage.putProductNameInSearchField("dress");
        Assertions.assertTrue(homepage.getNumberOfFoundProducts() == 7);
    }

    @Test
    void shouldRetryErrorByNonExistingProducts() {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        homepage.putProductNameInSearchField("violin");
        Assertions.assertTrue(homepage.alertByNoResultsIsDisplayed());

    }

    @Test
    void shouldSortProductsByPriceAscending() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        homepage.putProductNameInSearchField("dress");
        homepage.goToFashionManufacturerProducts();
        FashionManufacturerPage fashionManufacturerPage = new FashionManufacturerPage(driver, wait);
        Thread.sleep(5000);
        fashionManufacturerPage.chooseSortingSelector(SortParameters.PRICEASC);
        Assertions.assertEquals(fashionManufacturerPage.sortAscendingCurrentPrices(3), fashionManufacturerPage.getCurrentPrices(3));
    }

    @Test
    void shouldSortProductsByPriceDescending() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        homepage.putProductNameInSearchField("dress");
        homepage.goToFashionManufacturerProducts();
        FashionManufacturerPage fashionManufacturerPage = new FashionManufacturerPage(driver, wait);
        Thread.sleep(5000);
        fashionManufacturerPage.chooseSortingSelector(SortParameters.PRICEDESC);
        Assertions.assertNotEquals(fashionManufacturerPage.sortAscendingCurrentPrices(2), fashionManufacturerPage.getCurrentPrices(2));
        Assertions.assertEquals(fashionManufacturerPage.sortAscendingCurrentPrices(0), fashionManufacturerPage.getCurrentPrices(6));
    }
}

