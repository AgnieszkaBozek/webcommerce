package tests;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    static WebDriver driver;
    static WebDriverWait wait;

    @BeforeAll
    static void driverSetup() {

        driver = chooseDriver("chrome");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 5);
    }

    private static WebDriver chooseDriver(String browserName) {
        if (browserName.toLowerCase().contains("chrome")) {
            return driver = new ChromeDriver();
        } else if (browserName.toLowerCase().contains("firefox")) {
            driver = new FirefoxDriver();
        } else {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("disable-gpu");
            driver = new ChromeDriver(chromeOptions);
        }
        return driver;
    }

    @AfterAll
    static void closeDriver() {
        driver.quit();
    }

    @BeforeEach
    void clearCookies() {
        driver.manage().deleteAllCookies();
    }
}






