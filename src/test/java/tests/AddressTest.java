package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.MyAccountPage;

public class AddressTest extends BaseTest {


    @Test
    void shouldAddNewAddress()  {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wp.pl", "mojtest");
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        myAccountPage.provideNewAddress("Box 6 Ca.73", "Washington", "57485", "Alaska", "95959955", " adresiwo");
        Assertions.assertTrue(myAccountPage.newAddressBoxIsDisplayed());
    }

    @Test
    void shouldUpdateExistingAddress()  {

        HomePage homepage = new HomePage(driver, wait);
        homepage.openPage();
        LoginPage loginPage = homepage.goToLoginPage();
        loginPage.login("test@wp.pl", "mojtest");
        MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
        myAccountPage.updateAddress("Box 6 Ca.890");
        Assertions.assertEquals("Box 6 Ca.890", myAccountPage.getAddressLineFromMyAddressBox());
    }
}
