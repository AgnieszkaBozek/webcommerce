package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class TshirtsPage extends BasePage {


    List<String> sections = new ArrayList<>();

    List<String> sectionNames = new ArrayList<>();

    @FindBy(css = ".quick-view")
    private static WebElement quickViewButton;

    @FindBy(css = "li>.product-container")
    private static WebElement productContainer;

    @FindBy(xpath = "//*[@class='primary_block row']")
    private static WebElement quickViewBox;

    @FindBy(css = ".fancybox-iframe")
    private static WebElement iFrameQuickView;

    @FindBy(xpath = "//*[@class='socialsharing_product list-inline no-print']")
    private static WebElement panelWithSocialSharingButtons;

    @FindBy(xpath = "//*[@class='btn btn-default button-plus product_quantity_up']")
    private static WebElement plusButton;

    @FindBy(xpath = "//*[@class='btn btn-default button-minus product_quantity_down']")
    private static WebElement minusButton;

    @FindBy(xpath = "//*[@class='form-control attribute_select no-print']")
    private static WebElement sizeSelector;

    @FindBy(xpath = "//*[@name='qty']")
    private static WebElement quantity;

    @FindBy(css = "#uniform-group_1>span")
    private static WebElement sizeList;

    @FindBy(xpath = "//*[@class='button lnk_view btn btn-default']")
    private static WebElement moreButton;

    @FindBy(tagName = "h3")
    private static List<WebElement> productDetails;


    public TshirtsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void getQuickViewOfProduct() {
        Actions builder = new Actions(driver);
        builder.moveToElement(productContainer).build().perform();
        quickViewButton.click();
        WebElement quickViewIFrame = iFrameQuickView;
        wait.until(ExpectedConditions.visibilityOf(iFrameQuickView));
        iFrameQuickView.click();
        driver.switchTo().frame(iFrameQuickView);

    }

    public boolean quickViewIsDisplayed() {
        return panelWithSocialSharingButtons.isDisplayed();
    }

    public void changeQuantityOfProductInQuickView(String action, int number) {
        int counter = number;
        if (action.equals("+")) {
            do {
                plusButton.click();
                counter--;
            } while (counter > 0);
        }
        if (action.equals("-")) {
            do {
                minusButton.click();
                counter--;
            }
            while (counter > 0);
        }
    }

    public String getCurrentQuantity() {
        return quantity.getAttribute("value");
    }

    public void chooseSize(String size) {
        Select sizeSelect = new Select(sizeSelector);
        sizeSelect.selectByVisibleText(size);
    }

    public String chosenSizeIsDisplayed() {
        return sizeList.getText();
    }

    public void goToMoreDetails() {
        Actions builder = new Actions(driver);
        builder.moveToElement(productContainer).build().perform();
        moreButton.click();
    }


    public List<String> getSectionNames() {
        for (WebElement a : productDetails) {
            sections.add(a.getText());
        }
        return sections;
    }

    public List<String> displayExpectedSections() {
        sectionNames.add("DATA SHEET");
        sectionNames.add("MORE INFO");
        sectionNames.add("REVIEWS");
        return sectionNames;
    }
}

