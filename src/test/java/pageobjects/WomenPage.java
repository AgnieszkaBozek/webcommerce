package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class WomenPage extends BasePage {


    public WomenPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }


    @FindBy(css = ".wishlist")
    private static List<WebElement> addToWishlistButtons;

    @FindBy(css = ".product-container")
    private static List<WebElement> products;

    @FindBy(xpath = "//*[@title='Close']")
    private static WebElement closeButton;

    @FindBy(css = ".account")
    private static WebElement accountNameButton;

    @FindBy(xpath = "//*[@class='lnk_wishlist']")
    private static WebElement myWishLists;

    @FindBy(xpath = " //*[@class='fancybox-error']")
    private static WebElement addToWishListError;

    @FindBy(css = ".ajax_add_to_cart_button")
    private static List<WebElement> addToCartButtons;

    @FindBy(xpath = "//*[@title='Close window']")
    private static WebElement closeWindowButton;


    public void addRandomProductsToWishlist(int numberOfItems) throws InterruptedException {
        Random rnd = new Random();
        for (int i = 0; i < numberOfItems; i++) {
            int productIndex = rnd.nextInt(products.size());
            Actions builder = new Actions(driver);
            builder.moveToElement(products.get(productIndex)).build().perform();
            addToWishlistButtons.get(productIndex).click();
            closeButton.click();
        }
    }

    public void goToAccountWishListOption() {
        accountNameButton.click();
        myWishLists.click();
    }

    public String getErrorMessageByAddingProductToWishlist() {
        return addToWishListError.getText();
    }
}






