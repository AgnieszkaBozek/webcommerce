package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    protected static WebDriver driver;

    protected static WebDriverWait wait;

    private static final String BASE_URL = "http://automationpractice.com/index.php";


    public BasePage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.wait = wait;
    }

    public void openPage() {
        driver.get(BASE_URL);
    }
}
