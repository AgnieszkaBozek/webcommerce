package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ShoppingCartPage extends BasePage {

    @FindBy(xpath = "//*[@class='button btn btn-default standard-checkout button-medium']")
    private static WebElement checkoutButton;

    @FindBy(xpath = "//*[@type='checkbox']")
    private static WebElement tAndCCheckbox;

    @FindBy(xpath = "//*[@class='col-xs-12 col-md-6']")
    private static List<WebElement> paymentMethods;

    @FindBy(xpath = "//*[@class='button btn btn-default button-medium']")
    private static WebElement confirmOrderButton;

    @FindBy(xpath = "//*[@class='box order-confirmation']")
    private static WebElement orderCompleteMessage;

    public ShoppingCartPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void proceedToCheckout() {
        checkoutButton.click();
        confirmOrder();
    }

    public void choosePaymentMethod(String payBy) {

        if (payBy.toLowerCase().trim().contains("bank wire")) {
            paymentMethods.get(0).click();
        } else paymentMethods.get(1).click();
    }

    public void confirmOrder() {
        confirmOrderButton.click();
    }

    public void acceptTermsAndConditions() {
        tAndCCheckbox.click();
        checkoutButton.click();
    }

    public boolean orderCompleteMessageIsDisplayed() {
        return orderCompleteMessage.isDisplayed();
    }
}

