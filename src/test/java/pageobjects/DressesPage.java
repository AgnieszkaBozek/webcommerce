package pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;


public class DressesPage extends BasePage {


    private double cartSum = 0.0;

    @FindBy(css = ".ajax_add_to_cart_button")
    private static List<WebElement> addToCartButtons;

    @FindBy(xpath = "//*[@class= 'ajax_cart_quantity unvisible']")
    private static WebElement cartCounter;

    @FindBy(linkText = "Product successfully added to your shopping cart")
    private static WebElement messageAfterAddToCart;

    @FindBy(xpath = "//*[@title='Close window']")
    private static WebElement closeWindowButton;

    @FindBy(xpath = "//*[@class='product-container']")
    private static List<WebElement> products;

    @FindBy(xpath = "//div[@class='product-image-container']//span[@itemprop='price']")
    private static List<WebElement> prices;

    @FindBy(xpath = "//*[@id='total_product']")
    private static WebElement totalPrice;

    @FindBy(xpath = "//*[@title='View my shopping cart']")
    private static WebElement viewMyShoppingCartButton;

    public DressesPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public String getItemsNumber() {
        return cartCounter.getText();
    }

    public void addRandomProductsToCart(int numberOfItems) throws InterruptedException {
        Random rnd = new Random();
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,600)");
        wait.until(ExpectedConditions.visibilityOfAllElements(products));
        for (int i = 0; i < numberOfItems; i++) {
            int productIndex = rnd.nextInt(products.size());
            Actions builder = new Actions(driver);
            builder.moveToElement(products.get(productIndex)).build().perform();
            addToCartButtons.get(productIndex).click();
            closeWindowButton.click();
            wait.until(ExpectedConditions.visibilityOfAllElements(products));
        }
    }

    public void checkPriceForAddedRandomProducts(int numberOfItems) throws InterruptedException {
        Random rnd = new Random();
        wait.until(ExpectedConditions.visibilityOfAllElements(products));
        for (int i = 0; i < numberOfItems; i++) {
            int productIndex = rnd.nextInt(products.size());
            Actions builder = new Actions(driver);
            builder.moveToElement(products.get(productIndex)).build().perform();
            String productPrice = prices.get(productIndex).getText();
            productPrice = productPrice.replace("$", "");
            cartSum += Double.parseDouble(productPrice);
            addToCartButtons.get(productIndex).click();
            closeWindowButton.click();
            wait.until(ExpectedConditions.visibilityOfAllElements(products));
        }
    }

    public double getExpectedCartValue() {

        return roundCartSum(cartSum, 2);
    }

    public double getCurrentTotalValue() {
        wait.until(ExpectedConditions.elementToBeClickable(totalPrice));
        return Double.parseDouble(totalPrice.getText().replace("$", ""));
    }

    private static double roundCartSum(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void goToShoppingCartPage() {
        viewMyShoppingCartButton.click();
    }
}




