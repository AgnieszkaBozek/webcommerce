package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;

public class LoginPage extends BasePage {

    @FindBy(id = "email_create")
    private static WebElement emailRegisterField;

    @FindBy(id = "SubmitCreate")
    private static WebElement createAnAccountButton;

    @FindBy(id = "email")
    private static WebElement emailField;

    @FindBy(id = "passwd")
    private static WebElement passwordField;

    @FindBy(id = "SubmitLogin")
    private static WebElement signInButton;

    @FindBy(css = "div>ol")
    private static WebElement errorAlert;


    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void login(String email, String password) {
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        signInButton.click();
    }


    public void createAnAccount() {
        RandomUser rndUser = new RandomUser();
        emailRegisterField.sendKeys(rndUser.email);
        createAnAccountButton.sendKeys(Keys.ENTER);
    }

    public boolean createAnAccountButtonIsDisplayed() {
        return createAnAccountButton.isDisplayed();
    }

    public String getErrorTextByLoginAttempt() {
        return errorAlert.getText();
    }
}