package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.SortParameters;

import java.util.*;

public class FashionManufacturerPage extends BasePage {

    ArrayList<String> currentPrices = new ArrayList<>();

    ArrayList<String> pricesSortedAsc = new ArrayList<>();


    @FindBy(id = "selectProductSort")
    private static WebElement sortOptionsDropDown;

    @FindBy(xpath = "//div[@class='right-block']//span[@itemprop='price']")
    private static List<WebElement> prices;


    public FashionManufacturerPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void chooseSortingSelector(SortParameters type) {
        Select sortType = new Select(sortOptionsDropDown);
        switch (type) {
            case PRICEASC:
                sortType.selectByValue("price:asc");
                break;
            case PRICEDESC:
                sortType.selectByValue("price:desc");
                break;
            case QUANTITYDESC:
                sortType.selectByValue("quantity:desc");
                break;
            case NAMEASC:
                sortType.selectByValue("name:asc");
                break;
            case NAMEDESC:
                sortType.selectByValue("name:desc");
                break;
            case REFERENCEASC:
                sortType.selectByValue("reference:asc");
                break;
            case REFERENCEDESC:
                sortType.selectByValue("reference:desc");
                break;
            default:
                sortType.selectByValue("position:asc");
        }
    }

    public String getCurrentPrices(int index) {
        for (WebElement b : prices) {
            String price = b.getText().replace("$", "");
            currentPrices.add(price);
        }
        return currentPrices.get(index);
    }

    public String sortAscendingCurrentPrices(int index) {
        for (WebElement b : prices) {
            String price = b.getText().replace("$", "");
            pricesSortedAsc.add(price);
            Collections.sort(pricesSortedAsc);
        }
        return pricesSortedAsc.get(index);
    }
}


