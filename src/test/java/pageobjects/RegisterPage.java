package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;


public class RegisterPage extends BasePage {

    @FindBy(xpath = "//*[@for= 'id_gender2']")
    private static WebElement titleMrs;

    @FindBy(id = "customer_firstname")
    private static WebElement firstNameField;

    @FindBy(id = "customer_lastname")
    private static WebElement lastNameField;

    @FindBy(id = "passwd")
    private static WebElement passwordField;

    @FindBy(id = "address1")
    private static WebElement address1Field;

    @FindBy(id = "city")
    private static WebElement cityField;

    @FindBy(id = "id_state")
    private static WebElement stateDropDownField;

    @FindBy(id = "postcode")
    private static WebElement zipCodeField;

    @FindBy(id = "phone_mobile")
    private static WebElement mobilePhoneField;

    @FindBy(id = "submitAccount")
    private static WebElement registerButton;

    @FindBy(css = ".alert-danger")
    private static WebElement alertBanner;

    @FindBy(css = "ol>li")
    private static WebElement textInAlertBanner;

    public RegisterPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void fillInRegistrationForm() {

        RandomUser rndUser = new RandomUser();
        firstNameField.sendKeys(rndUser.firstName);
        lastNameField.sendKeys(rndUser.lastName);
        address1Field.sendKeys(rndUser.address);
        cityField.sendKeys(rndUser.city);
        passwordField.sendKeys(rndUser.password);
        zipCodeField.sendKeys(rndUser.zipCode);
        mobilePhoneField.sendKeys(rndUser.mobilePhone);
        Select state = new Select(stateDropDownField);
        state.selectByVisibleText("Arizona");
        registerButton.click();
    }

    public void provideZipCode(String zipCode) {

        RandomUser rndUser = new RandomUser();
        firstNameField.sendKeys(rndUser.firstName);
        lastNameField.sendKeys(rndUser.lastName);
        address1Field.sendKeys(rndUser.address);
        cityField.sendKeys(rndUser.city);
        passwordField.sendKeys(rndUser.password);
        zipCodeField.sendKeys(zipCode);
        mobilePhoneField.sendKeys(rndUser.mobilePhone);
        Select state = new Select(stateDropDownField);
        state.selectByVisibleText("Arizona");
        registerButton.click();
    }

    public void providePassword(String password) {
        RandomUser rndUser = new RandomUser();
        firstNameField.sendKeys(rndUser.firstName);
        lastNameField.sendKeys(rndUser.lastName);
        address1Field.sendKeys(rndUser.address);
        cityField.sendKeys(rndUser.city);
        passwordField.sendKeys(password);
        zipCodeField.sendKeys(rndUser.zipCode);
        mobilePhoneField.sendKeys(rndUser.mobilePhone);
        Select state = new Select(stateDropDownField);
        state.selectByVisibleText("Arizona");
        registerButton.click();
    }

    public String getErrorMessageByWrongDataProvided() {
        return textInAlertBanner.getText();
    }

    public void giveNumberOfRequiredFieldsThatStayEmpty(int numberOfEmptyFields) {

        RandomUser rndUser = new RandomUser();
        Select state = new Select(stateDropDownField);

        switch (numberOfEmptyFields) {

            case 1:
                lastNameField.sendKeys(rndUser.lastName);
                address1Field.sendKeys(rndUser.address);
                cityField.sendKeys(rndUser.city);
                passwordField.sendKeys(rndUser.password);
                zipCodeField.sendKeys(rndUser.zipCode);
                mobilePhoneField.sendKeys(rndUser.mobilePhone);
                state.selectByVisibleText("Arizona");
                registerButton.click();
                break;
            case 2:
                address1Field.sendKeys(rndUser.address);
                cityField.sendKeys(rndUser.city);
                passwordField.sendKeys(rndUser.password);
                zipCodeField.sendKeys(rndUser.zipCode);
                mobilePhoneField.sendKeys(rndUser.mobilePhone);
                state.selectByVisibleText("Arizona");
                registerButton.click();
                break;
            case 3:
                cityField.sendKeys(rndUser.city);
                passwordField.sendKeys(rndUser.password);
                zipCodeField.sendKeys(rndUser.zipCode);
                mobilePhoneField.sendKeys(rndUser.mobilePhone);
                state.selectByVisibleText("Arizona");
                registerButton.click();
                break;
            case 4:
                passwordField.sendKeys(rndUser.password);
                zipCodeField.sendKeys(rndUser.zipCode);
                mobilePhoneField.sendKeys(rndUser.mobilePhone);
                state.selectByVisibleText("Arizona");
                registerButton.click();
                break;
            case 5:
                zipCodeField.sendKeys(rndUser.zipCode);
                mobilePhoneField.sendKeys(rndUser.mobilePhone);
                state.selectByVisibleText("Arizona");
                registerButton.click();
                break;
            case 6:
                mobilePhoneField.sendKeys(rndUser.mobilePhone);
                state.selectByVisibleText("Arizona");
                registerButton.click();
                break;
            case 7:
                state.selectByVisibleText("Arizona");
                registerButton.click();
                break;
            case 8:
                registerButton.click();
                break;
        }
    }

    public boolean alertBannerIsDisplayed() {
        return alertBanner.isDisplayed();
    }
}



