package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class MyAccountPage extends BasePage {

    @FindBy(xpath = "//*[@title='Orders']")
    private static WebElement OrderHistoryAndDetailsText;

    @FindBy(linkText = "My addresses")
    private static WebElement MyAddressesOption;

    @FindBy(css = "a[title = 'Update']")
    private static WebElement UpdateButton;

    @FindBy(css = "a[title = 'Add an address']")
    private static WebElement addNewAddressButton;

    @FindBy(id = "submitAddress")
    private static WebElement saveButton;

    @FindBy(id = "address1")
    private static WebElement address1Field;

    @FindBy(css = ".address_address1")
    private static WebElement addressLine1;

    @FindBy(css = ".logout")
    private static WebElement signOutButton;

    @FindBy(id = "city")
    private static WebElement cityField;

    @FindBy(id = "id_state")
    private static WebElement stateDropDownField;

    @FindBy(id = "postcode")
    private static WebElement zipCodeField;

    @FindBy(id = "phone_mobile")
    private static WebElement mobilePhoneField;

    @FindBy(id = "alias")
    private static WebElement aliasField;

    @FindBy(xpath = "//*[@class='last_item alternate_item box']")
    private static WebElement newAddressBox;

    @FindBy(xpath = "//*[@class='lnk_wishlist']")
    private static WebElement myWishLists;

    @FindBy(xpath = "//*[@name='name']")
    private static WebElement wishListNameInputField;

    @FindBy(xpath = " //*[@name='submitWishlist']")
    private static WebElement saveNewWishListButton;

    @FindBy(xpath = " //*[@class='table table-bordered']")
    private static WebElement wishListTable;

    @FindBy(xpath = "//*[@class='bold align_center']")
    private static WebElement quantityFieldInWishList;

    public MyAccountPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void updateAddress(String address) {

        MyAddressesOption.click();
        UpdateButton.click();
        address1Field.clear();
        address1Field.sendKeys(address);
        saveButton.click();
    }

    public void provideNewAddress(String newAddress, String city, String zipCode, String newState, String mobile, String newAlias) {

        MyAddressesOption.click();
        addNewAddressButton.click();
        address1Field.sendKeys(newAddress);
        cityField.sendKeys(city);
        zipCodeField.sendKeys(zipCode);
        Select state = new Select(stateDropDownField);
        state.selectByVisibleText(newState);
        mobilePhoneField.sendKeys(mobile);
        aliasField.sendKeys(newAlias);
        saveButton.click();
    }

    public String getAddressLineFromMyAddressBox() {
        return addressLine1.getText();
    }

    public boolean newAddressBoxIsDisplayed() {
        return newAddressBox.isDisplayed();
    }

    public boolean orderHistoryOptionIsDisplayed() {
        return OrderHistoryAndDetailsText.isDisplayed();
    }

    public void signOut() {
        signOutButton.click();
    }

    public void createAWishList(String listName) {
        myWishLists.click();
        wishListNameInputField.sendKeys(listName);
        saveNewWishListButton.click();
    }

    public boolean wishListCreated() {
        return wishListTable.isDisplayed();
    }

    public String getNumberOfProductsInWishList() {
        return quantityFieldInWishList.getText();
    }
}
