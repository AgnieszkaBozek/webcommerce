package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HomePage extends BasePage {

    @FindBy(css = ".login")
    private static WebElement signInButton;

    @FindBy(css = "#block_top_menu>ul>li")
    private static List<WebElement> menuTabs;

    @FindBy(xpath = "//a[@title='Women']")
    private static WebElement womenTab;

    @FindBy(xpath = "//*[@id='block_top_menu']/ul/li[2]/a")
    private static WebElement dressesTab;

    @FindBy(css = "#block_top_menu > ul > li:nth-child(3) > a")
    private static WebElement tShirtsTab;

    @FindBy(id = "search_query_top")
    private static WebElement searchField;

    @FindBy(css = ".button-search")
    private static WebElement buttonSearch;

    @FindBy(xpath = "//a[@title='Tops']")
    private static WebElement tops;

    @FindBy(css = ".alert")
    private static WebElement errorNoSearchResults;

    @FindBy(xpath = "//div[@class='product-image-container']")
    private static List<WebElement> numberOfProductsAfterSearch;

    @FindBy(xpath = "//*[@class='sf-menu clearfix menu-content sf-js-enabled sf-arrows']//*[@class='submenu-container clearfix first-in-line-xs']")
    private static List<WebElement> subCategories;

    @FindBy(xpath = "//*[@title='Manufacturers']")
    private static WebElement manufacturersSection;


    @FindBy(xpath = "//*[@class='btn btn-default button exclusive-medium']")
    private static WebElement viewProductsButton;


    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public LoginPage goToLoginPage() {
        signInButton.click();
        LoginPage loginpage = new LoginPage(driver, wait);
        return loginpage;
    }

    public DressesPage goToDressesPage() {
        dressesTab.click();
        DressesPage dressesPage = new DressesPage(driver, wait);
        return dressesPage;
    }

    public WomenPage goToWomenPage() {
        womenTab.click();
        WomenPage womenPage = new WomenPage(driver, wait);
        return womenPage;
    }

    public TshirtsPage goToTshirtsPage() {
        tShirtsTab.click();
        TshirtsPage tshirtsPage = new TshirtsPage(driver, wait);
        return tshirtsPage;
    }

    public int countMenuTabs() {
        return menuTabs.size();
    }


    public void getSubcategories(String categoryName) throws InterruptedException {
        Actions builder = new Actions(driver);
        Wait wait = new WebDriverWait(driver, 5000);
        if (categoryName.toLowerCase().contains("women")) {
            builder.moveToElement(womenTab).build().perform();
            wait.until(ExpectedConditions.visibilityOf(subCategories.get(0)));

        } else if (categoryName.toLowerCase().contains("dresses")) {
            builder.moveToElement(dressesTab).build().perform();
            wait.until(ExpectedConditions.visibilityOf(subCategories.get(1)));
        } else {
            builder.moveToElement(tShirtsTab).build().perform();
            System.out.println("No subcategories, assertion should be false");
        }

    }

    public boolean subcategoriesAreVisible(int categoryIndex) {
        subCategories.get(categoryIndex).isDisplayed();
        return true;
    }

    public void putProductNameInSearchField(String productName) {
        searchField.sendKeys(productName);
        buttonSearch.click();
    }

    public boolean alertByNoResultsIsDisplayed() {
        return errorNoSearchResults.isDisplayed();
    }

    public int getNumberOfFoundProducts() {
        return numberOfProductsAfterSearch.size();
    }

    public void goToFashionManufacturerProducts() {
        manufacturersSection.click();
        viewProductsButton.click();

    }
}



