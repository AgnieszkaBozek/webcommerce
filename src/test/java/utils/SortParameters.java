package utils;

public enum SortParameters {
    PRICEASC,
    PRICEDESC,
    NAMEASC,
    NAMEDESC,
    QUANTITYDESC,
    REFERENCEASC,
    REFERENCEDESC,
}
