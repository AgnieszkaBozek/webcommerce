package utils;

import com.github.javafaker.Faker;

public class RandomUser {

    public String username;
    public String email;
    public String password;
    public String firstName;
    public String lastName;
    public String company;
    public String address;
    public String city;
    public String zipCode;
    public String mobilePhone;
    Faker faker;


    public RandomUser() {
        faker = new Faker();
        username = faker.name().username();
        email = username + faker.number().randomDigit() + "@kx.pl";
        password = "Qaz#edc$fg";
        firstName = faker.name().firstName();
        lastName = faker.name().lastName();
        company = faker.company().name();
        address = faker.address().streetAddress();
        city = faker.address().cityName();
        zipCode = faker.number().digits(5);
        mobilePhone = faker.phoneNumber().cellPhone();
    }
}

