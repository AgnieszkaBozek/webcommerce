# WEB UI TESTS #

This repository is for automated functional tests of page http://automationpractice.com as a final project for automatic tester course provided by SOFTIE (https://softie.pl/).

### OVERVIEW ###

Technologies used: 
Java 11+,
Maven, 
Selenium WebDriver, 
jUnit 5.x, 
Page object pattern, 
Page factory. 


### SETUP ###
In order to run test you need to have chrome webdriver and firexof webdriver on local machine. 

Chrome webdriver can be downloaded from here: https://chromedriver.chromium.org/downloads

Firefox webdriver you can find here: https://github.com/mozilla/geckodriver/releases

**IMPORTANT!**
**Your browser version must be the same as driver you will download.**


### PAGE OBJECT PATTERN ###

-**HomePage**
![HomePage] (docs/HomePageObject.png)

-**LoginPage**
![LoginPage] (docs/LoginPageObject.png)


